# coding=utf-8
import collections
import json

from django.views.generic import TemplateView, View
from django.http import HttpResponse

from gridoptimizer import gridoptimizer

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

plants_json = [
  # {
  #   	"plant_name": "noplant",
		# "common_name": "Noplant",
		# "scientific_name": "Solanum melongena",
		# "friends": [],
		# "image": "noplant.jpg",
		# "video_links": [],
		# "sunlight": "more",
		# "weather": [],
		# "qty_psq": 1
  # },
	{
		"plant_name": "eggplant",
		"common_name": "Eggplant",
		"scientific_name": "Solanum melongena",
		"friends": ["beans", "okra"],
		"image": "eggplant.jpg",
		"video_links": ["13xAS6ebfBk"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "peas",
		"common_name": "Peas",
		"scientific_name": "Pisum sativum",
		"friends": ['corn'],
		"image": "pea.jpg",
		"video_links": ["bRqd6hLbOnY"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 16,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "beet",
		"common_name": "Beet",
		"scientific_name": "Beta vulgaris",
		"friends": ["cabbage", "onion"],
		"image": "beet.jpg",
		"video_links": ["Uk_YzY_CPpY"],
		"sunlight": "less",
		"weather": ["winter"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "raddish",
		"common_name": "Raddish",
		"scientific_name": "Raphanus sativus",
		"friends": ['cucumber', 'okra'],
		"image": "radish.jpg",
		"video_links": ["9UlLcUf-JCA"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "lettuce",
		"common_name": "Lettuce",
		"scientific_name": "Lactuca sativa",
		"friends": ["carrot", "onion", "raddish"],
		"image": "lettuce.jpg",
		"video_links": ["f0JNK8de6xo"],
		"sunlight": "less",
		"weather": ["summer", "winter"],
		"qty_psq": 4,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "cauliflower",
		"common_name": "Cauliflower",
		"scientific_name": "Brassica oleracea",
		"friends": ["beans"],
		"image": "cauliflower.jpg",
		"video_links": ["fYTO7dX13sI"],
		"sunlight": "less",
		"weather": ["summer"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "onion",
		"common_name": "Onion",
		"scientific_name": "Allium cepa",
		"friends": ["beet", "tomato", "lettuce", "cabbage", "carrot", "capsicum"],
		"image": "onion.jpg",
		"video_links": ["2R0kzCwGNtw"],
		"sunlight": "more",
		"weather": ["summer"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "tomato",
		"common_name": "Tomato",
		"scientific_name": "Solanum lycopersicum",
		"friends": ["onion", "chives"],
		"image": "tomato.jpg",
		"video_links": ["RCfF8O2Azww"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "carrot",
		"common_name": "Carrot",
		"scientific_name": "Daucus carota",
		"friends": ["chives", "beans", "peas", "lettuce", "onion", "capsicum", "bittergourd", "bottlegourd"],
		"image": "carrot.jpg",
		"video_links": ["UW4zNQcj4Tk"],
		"sunlight": "more",
		"weather": ["summer"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "okra",
		"common_name": "Okra",
		"scientific_name": "Abelmoschus esculentus",
		"friends": ['eggplant', 'beans', 'cucumber', 'raddish'],
		"image": "okra.jpg",
		"video_links": ["1noUafH8YsY"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "cucumber",
		"common_name": "Cucumber",
		"scientific_name": "Cucumis sativus",
		"friends": ['raddish', 'okra'],
		"image": "cucumber.jpg",
		"video_links": ["dLCX6LQ4q4c"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "spinach",
		"common_name": "Spinach",
		"scientific_name": "Spinacia oleracea",
		"friends": [],
		"image": "spinach.jpg",
		"video_links": ["glzA3aH35l4"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "capsicum",
		"common_name": "Capsicum",
		"scientific_name": "Capsicum annuum",
		"friends": ["carrot", "onion"],
		"image": "capsicum.jpg",
		"video_links": ["8b_3zXPjcjI"],
		"sunlight": "more",
		"weather": ["summer"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "cabbage",
		"common_name": "Cabbage",
		"scientific_name": "Brassica oleracea",
		"friends": ["beans", "beet", "onion"],
		"image": "cabbage.jpg",
		"video_links": ["Cnr-ZSpvACs"],
		"sunlight": "more",
		"weather": ["winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "beans",
		"common_name": "Beans",
		"scientific_name": "Phaseolus vulgaris",
		"friends": ['eggplant', 'okra', 'corn'],
		"image": "beans.jpg",
		"video_links": ["jx9A0iIV4dM"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "bittergourd",
		"common_name": "Bitter Gourd",
		"scientific_name": "Momordica charantia",
		"friends": ["carrot", "raddish"],
		"image": "bittergourd.jpg",
		"video_links": ["H2kpQ5zBAo0"],
		"sunlight": "more",
		"weather": ["winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "bottlegourd",
		"common_name": "Bottle Gourd",
		"scientific_name": "Lagenaria siceraria",
		"friends": ["carrot", "raddish"],
		"image": "bottlegourd.jpg",
		"video_links": ["5jL0dpqQ6cw"],
		"sunlight": "more",
		"weather": ["winter"],
		"qty_psq": 1,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "fennel",
		"common_name": "Fennel",
		"scientific_name": "Foeniculum vulgare",
		"friends": [],
		"image": "fennel.jpg",
		"video_links": ["jbKGYbnH_Lg"],
		"sunlight": "more",
		"weather": ["winter"],
		"qty_psq": 16,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "chives",
		"common_name": "Chives",
		"scientific_name": "Allium schoenoprasum",
		"friends": ["carrot", "tomato"],
		"image": "chives.jpg",
		"video_links": ["uKv5FVoYxsI"],
		"sunlight": "more",
		"weather": ["winter"],
		"qty_psq": 9,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
		"plant_name": "corn",
		"common_name": "Corn",
		"scientific_name": "Zea mays",
		"friends": ['peas', 'beans'],
		"image": "corn.jpg",
		"video_links": ["A5AU2r10Ul4"],
		"sunlight": "more",
		"weather": ["summer", "winter"],
		"qty_psq": 4,
		"buy_links": [
			"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
			"http://biocarve.com/"
		]
	},
	{
    "plant_name": "ashgourd",
    "common_name": "Ashgourd",
    "scientific_name": "",
    "friends": [],
    "image": "ashgourd.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "cilanto",
    "common_name": "Cilanto",
    "scientific_name": "",
    "friends": [],
    "image": "cilanto.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "chillies",
    "common_name": "Chillies",
    "scientific_name": "",
    "friends": [],
    "image": "chillies.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "knolkol",
    "common_name": "Knolkol",
    "scientific_name": "",
    "friends": [],
    "image": "knolkol.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "mushroom",
    "common_name": "Mushroom",
    "scientific_name": "",
    "friends": [],
    "image": "mushroom.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "potato",
    "common_name": "Potato",
    "scientific_name": "",
    "friends": [],
    "image": "potato.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "tamarind",
    "common_name": "Tamarind",
    "scientific_name": "",
    "friends": [],
    "image": "tamarind.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "drumstick",
    "common_name": "Drumstick",
    "scientific_name": "",
    "friends": [],
    "image": "drumstick.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "sweetpotato",
    "common_name": "Sweet Potato",
    "scientific_name": "",
    "friends": [],
    "image": "sweetpotato.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "ridgegourd",
    "common_name": "Ridgegourd",
    "scientific_name": "",
    "friends": [],
    "image": "ridgegourd.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "tapioca",
    "common_name": "Tapioca",
    "scientific_name": "",
    "friends": [],
    "image": "tapioca.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "curryleaves",
    "common_name": "Curry leaves",
    "scientific_name": "",
    "friends": [],
    "image": "curryleaves.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "artichoke",
    "common_name": "Artichoke",
    "scientific_name": "",
    "friends": [],
    "image": "artichoke.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  },
  {
    "plant_name": "pumpkin",
    "common_name": "Pumpkin",
    "scientific_name": "",
    "friends": [],
    "image": "pumpkin.jpg",
    "video_links": [],
    "sunlight": "more",
    "weather": [],
    "qty_psq": 1,
    "buy_links": [
    	"http://nurserylive.com/buy-all-vegetable-garden-seeds-online-in-india",
    	"http://biocarve.com/"
    ]
  }
]



class CommonMixin(object):
  @staticmethod
  def append_form_error_class(forms):
    if not isinstance(forms, (list, tuple)):
      forms = [forms]
    for form in forms:
      for field in form.errors.iterkeys():
        try:
          css_class = form.fields[field].widget.attrs.get('class', '')
          form.fields[field].widget.attrs['class'] = css_class + ' error'
        except KeyError:
          pass

  @staticmethod
  def append_formset_error_class(formset):
    for form in formset:
      for field in form.errors.iterkeys():
        try:
          css_class = form.fields[field].widget.attrs.get('class', '')
          form.fields[field].widget.attrs['class'] = css_class + ' error'
        except KeyError:
          pass


class AppView(TemplateView):
  template_name = 'index.html'


class GetPlantDataView(View):
  def get(self, request):
    return HttpResponse(json.dumps(plants_json))


class OptimizeGrid(View):
  @method_decorator(csrf_exempt)
  def dispatch(self, request, *args, **kwargs):
    return super(OptimizeGrid, self).dispatch(request, *args, **kwargs)

  def post(self, request, *args, **kwargs):
  	inputData = json.loads(request.body)
  	length = inputData['length']
  	breadth = inputData['breadth']
  	plants_data = inputData['data']
  	plants_dct_map = {}
  	for item in plants_json:
  		plants_dct_map[item['plant_name']] = item
  	friends_map = {}
  	for p,v in plants_data.iteritems():
  		friends_map[p] = plants_dct_map[p]['friends']
  	data = gridoptimizer.RunOptimizer(length, breadth, plants_data, friends_map)
  	results = []
  	for d in data:
  		results.append(plants_dct_map[d])
  	return HttpResponse(json.dumps(results))

