"""organicgardening URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static


from . import views

urlpatterns = [
    url(r'^$', views.AppView.as_view(), name='index_url'),
    url(r'^admin/', admin.site.urls),
    url(r'^getPlantData/', views.GetPlantDataView.as_view()),
    url(r'^optimizeGrid/', views.OptimizeGrid.as_view()),
] + static(settings.FE_URL, document_root=settings.FE_ROOT) + static(settings.BOWER_URL, document_root=settings.BOWER_ROOT) + static(settings.SCRIPTS_URL, document_root=settings.SCRIPTS_ROOT) + static(settings.VIEWS_URL, document_root=settings.VIEWS_ROOT) + static(settings.STYLES_URL, document_root=settings.STYLES_ROOT) + static(settings.IMAGES_URL, document_root=settings.IMAGES_ROOT)
