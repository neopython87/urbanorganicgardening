'use strict';

/**
 * @ngdoc function
 * @name urbanGardeningApp.controller:WelcomeCtrl
 * @description
 * # WelcomeCtrl
 * Controller of the urbanGardeningApp
 */
angular.module('urbanGardeningApp')
  .controller('WelcomeCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
