'use strict';


function DialogCtrl($scope, $mdDialog, $sce, plantData) {
  this.mdDialog = $mdDialog;
  this.plant = plantData;
  this.videoLink = $sce.trustAsResourceUrl('http://www.youtube.com/embed/' + this.plant.video_links[0]);
}


function finishCtrl() {

};


/**
 * @ngdoc function
 * @name urbanGardeningApp.controller:GridCtrl
 * @description
 * # GridCtrl
 * Controller of the urbanGardeningApp
 */
angular.module('urbanGardeningApp')
  .controller('GridCtrl', function ($mdSidenav, $mdDialog, $q, data) {
    this.selectedBed= 0;
    this.mdSidenav = $mdSidenav;
    this.mdDialog = $mdDialog;
    this.data_ = data;

    if(data.optimizedPlantData) {
      return $q.resolve(data.optimizedPlantData)
    } else {
      data.optimizeGrid().then(function(data) {
        this.plantsData = data;
        this.sidebarPlants = angular.copy(this.data_.plantData);
      }.bind(this));
    }

    this.bedLength = data.currentSelectedBed.length;
    this.bedBreadth = data.currentSelectedBed.breadth;
    this.gridWidth = this.bedLength * 257;
    this.getRange = function() {
      return this.plantsData;
      // return new Array(this.bedLength * this.bedBreadth);
    };
    this.editPlant = function(index) {
      this.editingPlantIndex = index;
      $mdSidenav('left').open();
    };

    this.showInfo = function(ev, plantData) {
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'dialog',
        templateUrl: '/views/plant-info-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false,
        locals: {
          plantData: plantData
        }
      })
      .then(function(answer) {
        // this.status = 'You said the information was "' + answer + '".';
      }, function() {
        // this.status = 'You cancelled the dialog.';
      });
    };

     this.finish = function(ev) {
      $mdDialog.show({
        controller: finishCtrl,
        controllerAs: 'finish',
        templateUrl: '/views/finish-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false
      })
      .then(function(answer) {
        // this.status = 'You said the information was "' + answer + '".';
      }, function() {
        // this.status = 'You cancelled the dialog.';
      });
    };

    this.closeSidenave = function(data) {
      this.mdSidenav('left').close()
          .then(function() {
            this.plantsData[this.editingPlantIndex] = data.plant;
          }.bind(this));
    };

  });
