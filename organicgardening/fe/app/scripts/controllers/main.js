'use strict';

var pinCodeDialog = function(data, mdDialog) {
  this.bedLength = 5;
  this.bedBreadth = 5;
  var keys = Object.keys(data.pincodeMap)
  this.pincodes = Object.keys(data.pincodeMap);
  this.filterPlants = function() {
    var filteredPlants = data.getPlantsByPin(this.pinCode);
    mdDialog.hide({
      plants: filteredPlants,
      bedLength: this.bedLength,
      bedBreadth: this.bedBreadth
    });
  };
};

function DialogCtrl($scope, $mdDialog, $sce, plantData) {
  this.mdDialog = $mdDialog;
  this.plant = plantData;
  this.videoLink = $sce.trustAsResourceUrl('http://www.youtube.com/embed/' + this.plant.video_links[0]);
}


/**
 * @ngdoc function
 * @name urbanGardeningApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the urbanGardeningApp
 */
angular.module('urbanGardeningApp')
  .controller('MainCtrl', function ($mdDialog, $mdSidenav, data) {
    this.mdDialog = $mdDialog;
    this.selectedPlants = [];
    this.showDialog = true;
    data.getData().then(function(data) {
      this.plantsData = data;
    }.bind(this));

    this.mdDialog.show({
        controller: pinCodeDialog,
        controllerAs: 'pincodedialog',
        templateUrl: '/views/pincode-dialog.html',
        parent: angular.element(document.body),
        // targetEvent: ev,
        locals: {
          mdDialog: this.mdDialog
        },
        clickOutsideToClose:false,
        fullscreen: false
      })
      .then(function(response) {
        this.showDialog = false;
        this.plantsData = response.plants;
        this.setBedDimensions(+response.bedLength, +response.bedBreadth);
        this.bedGroups = data.getGroups();
        console.log('bedArray ', this.bedGroups);
        this.userPreferences = data.setUserPreferences(this.bedGroups);
        this.editBeds();
        this.setSelectedBed(this.userPreferences[0]);
        // data.setSelectedBed(this.bedGroups[0]);
        // this.filterPlantsByLight('less');
      }.bind(this), function() {
      }.bind(this));

    this.filterPlantsByLight = function(light) {
      if (light == 'more') {
        return;
      }
      this.plantsData =  this.plantsData.filter(function(plant) {
        return plant.sunlight == light;
      }.bind(this));
    }

    this.selectPlant = function(plant) {
      if (data.currentSelectedBed.numberOfBlocks != data.currentSelectedBed.plants.length ) {
        data.currentSelectedBed.plants.push(plant);
      }
    };

    this.removePlant = function(plant) {
      for (var count = 0; count < data.currentSelectedBed.plants.length; count++) {
         if (data.currentSelectedBed.plants[count].plant_name == plant.plant_name) {
          data.currentSelectedBed.plants.splice(count, 1);
          break;
         }
      }
    }

    this.getPlantCount = function(plant) {
      if (!data.currentSelectedBed || !data.currentSelectedBed.plants) return;
      return data.currentSelectedBed.plants.filter(function(p) {
        return p.plant_name == plant.plant_name;
      }).length;
    }

    this.remainingCells = function() {
      var value = "Total available slots: ";
      if (data.currentSelectedBed && data.currentSelectedBed.plants) {
        return value + (data.currentSelectedBed.numberOfBlocks - data.currentSelectedBed.plants.length);
      }
    }

    this.showNextButton = function() {
      if (data.currentSelectedBed && data.currentSelectedBed.plants) {
        var blocks = data.currentSelectedBed.numberOfBlocks - data.currentSelectedBed.plants.length;
      }
      return blocks != data.bedDetails[0].numberOfBlocks;
    }

    this.bedLength = 0;

    this.bedBreadth = 0;

    this.dataSetvice = data
    this.setBedDimensions = function(l, b) {
      this.dataSetvice.setTotalDimensions(l, b);
    };

    this.setSelectedBed = function(card) {
      this.selectedBed = card.bedId;
      data.setSelectedBed(card);
      console.log('data from service ', data.currentSelectedBed);
    };

    this.editBeds = function() {
      $mdSidenav('right').open();
    };

    this.closeSidenav = function() {
      $mdSidenav('right').close();
    };

    this.showInfo = function(ev, plantData) {
      $mdDialog.show({
        controller: DialogCtrl,
        controllerAs: 'dialog',
        templateUrl: '/views/plant-info-dialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false,
        locals: {
          plantData: plantData
        }
      })
      .then(function(answer) {
        // this.status = 'You said the information was "' + answer + '".';
      }, function() {
        // this.status = 'You cancelled the dialog.';
      });
    };
  });
