'use strict';

/**
 * @ngdoc function
 * @name urbanGardeningApp.controller:MarketplaceCtrl
 * @description
 * # MarketplaceCtrl
 * Controller of the urbanGardeningApp
 */
angular.module('urbanGardeningApp')
  .controller('MarketplaceCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
