'use strict';

/**
 * @ngdoc service
 * @name urbanGardeningApp.data
 * @description
 * # data
 * Service in the urbanGardeningApp.
 */
angular.module('urbanGardeningApp')
  .service('data', function ($http, $cookies, $q) {

    this.q_ = $q;
    this.cookies = $cookies;

    this.totalLength;

    this.totalBreadtg;

  	// Angular http service const.
  	this.http_ = $http;

  	// Plants data fetched from backend.
  	this.plantData = [];

  	// Default user preferences.
  	this.userPreferences = {
  		availableArea: {length: 0, breadth: 0},
  		numberOfBeds: 0,
  	};

		// Bed details like id, plants, no of blocks, available blocks.
  	this.bedDetails = [];

    this.bedTotalLength = 0;

    this.bedTotalBreadth = 0;

    this.currentSelectedBed = {};
  	// Map of pincode to weather condition.
  	this.pincodeMap = {
  		'110001': 'winter', '110002': 'winter', '110003': 'winter',
  		'110004': 'winter', '110005': 'winter', '110006': 'winter',
  		'110007': 'winter', '110008': 'winter', '110009': 'winter',
  		'110010': 'winter', '110011': 'summer', '110012': 'summer',
  		'110013': 'summer', '110014': 'summer', '110015': 'summer',
  		'110016': 'summer', '110017': 'summer', '110018': 'summer',
  		'110019': 'summer', '110020': 'summer'
  	};

		// Gets plant data from backend and caches it.
  	this.getData = function() {
  		return this.http_.get('/getPlantData', {cache: true}).then(function(response) {
  			return this.plantData = response.data;
  		}.bind(this));
  	};

  	// Get plants by given pin code.
  	this.getPlantsByPin = function(pincode) {
      var plants = [];
      var weather = this.pincodeMap[pincode] || '';
      this.plantData.forEach(function(plant) {
        if (plant.weather.indexOf(weather) != -1) {
          plants.push(plant);
        }
      });
      return plants;
    };

		// Set user preferences when user enters available space.
  	this.setUserPreferences = function(bedDetails) {
  		this.userPreferences.availableArea.length = this.bedTotalLength;
  		this.userPreferences.availableArea.breadth = this.bedToalBreadth;
  		this.userPreferences.numberOfBeds = bedDetails.length;
      this.bedDetails = [];
  		for(var count = 0; count < bedDetails.length; count++) {
  				this.bedDetails.push({
            length: +this.bedTotalLength,
            breadth: Math.floor(bedDetails[count].breadth),
  					bedId: count + 1,
  					numberOfBlocks:
  							bedDetails[count].length * bedDetails[count].breadth,
  					availableBlocks:
  							bedDetails[count].length * bedDetails[count].breadth,
  					isSelected: false,
  					amount_of_sunshine: 'more',
  					plants: []
  				});
  		}
      console.log('debdetails ', this.bedDetails);
      return this.bedDetails;
  	};

    this.setSelectedBed = function(bedObj) {
      this.currentSelectedBed = bedObj;
    }

    this.setTotalDimensions = function(length, breadth) {
      this.bedTotalLength = length;
      this.bedTotalBreadth = breadth;
    };

    this.getTotalDimension = function() {

    };

  	// Set current selected bed and sunshine available.
  	this.updateBedDetails = function(selectedBedId, sunshine) {
  		this.bedDetails.forEach(function(bed) {
  			if (bed.bedId == selectedBedId) {
  				bed.isSelected = true,
  				bed.amount_of_sunshine = sunshine
  			} else {
  				bed.isSelected = false
  			}
  		});
  	};

  	// Update plants order on insertion and deletion.
  	this.updatePlants = function(bedId, plants) {
  		this.bedDetails.forEach(function(bed) {
  			if (bed.bedId == bedId) {
  				bed.plants = plants;
  			}
  		});
  	};

  	this.optimizeGrid = function() {
  	  var data = {};
  	  var plants = this.currentSelectedBed.plants;
  	  plants.forEach(function(p) {
        if (!data.hasOwnProperty(p.plant_name)) {
          data[p.plant_name] = 0;
        }
        data[p.plant_name] += 1;
  	  });
  	  return this.http_.post(
  	    "/optimizeGrid/",
	      {
	        data: data,
	        length: this.currentSelectedBed.length,
	        breadth: this.currentSelectedBed.breadth
	      }).then(function(response) {
    			this.optimizedPlantData = response.data;
    			return this.optimizedPlantData;
  		}.bind(this));
  	};
  this.getGroups = function() {
    var breadth = this.bedTotalBreadth;
    var bedArray = [];
    var bedMap = {
      "fours": 0,
      "remainingBreadth": 0
    }
    if (breadth > 16) {
      var quotient = Math.floor(breadth / 4)
      var remainder = breadth % 4;
      var totalGroups = quotient;
      var deduction = totalGroups * 0.5;
      var leftOver = breadth - deduction;
      var leftOverFours = Math.floor(leftOver / 4);
      var leftOverLast = leftOver % 4;
      for (var p = 1; p <= leftOverFours; p++) {
        bedArray.push({"breadth": 4, "length": +this.bedTotalLength})
      }
      bedArray.push({"breadth": Math.floor(leftOverLast), "length": +this.bedTotalLength});
    }  else {
      var quotient = Math.floor(breadth / 4)
      var remainder = breadth % 4;
      var totalGroups = quotient + 1;
      var deduction = totalGroups * 0.5;
      var leftOver = breadth - deduction;
      var leftOverFours = Math.floor(leftOver / 4);
      var leftOverLast = leftOver % 4;
      for (var p = 1; p <= leftOverFours; p++) {
        bedArray.push({"breadth": 4, "length": +this.bedTotalLength})
      }
      bedArray.push({"breadth": Math.floor(leftOverLast), "length": +this.bedTotalLength});
    }

    return bedArray;
  };
  });
