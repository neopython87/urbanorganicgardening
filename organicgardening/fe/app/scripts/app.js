'use strict';

/**
 * @ngdoc overview
 * @name urbanGardeningApp
 * @description
 * # urbanGardeningApp
 *
 * Main module of the application.
 */
angular
  .module('urbanGardeningApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial'
  ])
  .config(function ($routeProvider, $mdThemingProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/welcome.html',
        controller: 'WelcomeCtrl',
        controllerAs: 'welcome'
      })
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/grid', {
        templateUrl: 'views/grid.html',
        controller: 'GridCtrl',
        controllerAs: 'grid'
      })
      .when('/beds', {
        templateUrl: 'views/beds.html',
        controller: 'BedsCtrl',
        controllerAs: 'beds'
      })
      .when('/marketplace', {
        templateUrl: 'views/marketplace.html',
        controller: 'GridCtrl',
        controllerAs: 'grid'
      })
      .otherwise({
        redirectTo: '/'
      });

    $mdThemingProvider.theme('default')
      .primaryPalette('teal')
      .accentPalette('purple');

  }).run(function ($rootScope) {
    $rootScope.$on('$locationChangeSuccess', function () {
        var page = location.hash.replace('#/', 'urban');
        $(document.body).addClass(page);
    });
});
