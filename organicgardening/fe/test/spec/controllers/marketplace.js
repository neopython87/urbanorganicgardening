'use strict';

describe('Controller: MarketplaceCtrl', function () {

  // load the controller's module
  beforeEach(module('urbanGardeningApp'));

  var MarketplaceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MarketplaceCtrl = $controller('MarketplaceCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MarketplaceCtrl.awesomeThings.length).toBe(3);
  });
});
