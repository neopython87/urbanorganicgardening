

class Plant(object):

  def __init__(self, plant_id, num_selected):
    self.plant_id = plant_id
    self.planted_grid_pos = [] # Instance of GridPlantLayout
    self.num_selected = num_selected
    self.current_weightage = 0
    self.friend_ids = []

  def GetAvailability(self):
    return self.num_selected - len(self.planted_grid_pos)


class GridPlantLayout(object):

  def __init__(self, x, y):
    self.posx = x
    self.posy = y
    self.neighbor_coordinates = []  # Tuple [(x1,y1), (x2,y2)]
    self.associated_plant = None  #  Plant instance aassociated with this pos
    self.available_neighbor_slots = 0


total_selection = 0
selection_done = 0