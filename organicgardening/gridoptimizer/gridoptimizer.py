
import math
import collections
import operator

import datamodels


# selected_plants = collections.OrderedDict([
#   ('eggplant', 2),
#   ('corn', 4),
#   ('cucumber', 3),
#   ('fennel', 1),
#   ('peas', 2),
#   ('beans', 2),
#   ('raddish', 1),
#   ('okra', 3)
# ])

# friends_map = {
#   'eggplant': ['beans', 'okra'],
#   'corn': ['cucumber', 'raddish', 'peas'],
#   'cucumber': ['corn', 'raddish', 'okra'],
#   'fennel': [],
#   'peas': ['corn'],
#   'beans': ['eggplant', 'okra'],
#   'raddish': ['corn', 'cucumber'],
#   'okra': ['eggplant', 'cucumber', 'beans']
# }

# selected_plants = collections.OrderedDict([
#   ('p1', 1),
#   ('p2', 1),
#   ('p3', 4),
#   ('p4', 3),
#   ('p5', 3)
# ])

# friends_map = {
#   'p1': ['p2', 'p4'],
#   'p2': ['p1'],
#   'p3': ['p5'],
#   'p4': ['p1'],
#   'p5': ['p3']
# }


# selected_plants = collections.OrderedDict([
#   ('eggplant', 1),
#   ('peas', 3),
#   ('beans', 4),
#   ('cucumber', 2),
#   ('raddish', 2),
#   ('okra', 4),
#   ('corn', 4)
# ])

# friends_map = {
#   'eggplant': ['beans', 'okra'],
#   'peas': ['corn'],
#   'beans': ['eggplant', 'okra', 'corn'],
#   'cucumber': ['raddish', 'okra'],
#   'raddish': ['cucumber', 'okra'],
#   'okra': ['eggplant', 'beans', 'cucumber', 'raddish'],
#   'corn': ['peas', 'beans'],
# }

# friends_map = {
#   'eggplant': ['beans', 'okra'],
#   'corn': ['cucumber', 'raddish', 'peas'],
#   'cucumber': ['corn', 'raddish', 'okra'],
#   'fennel': [],
#   'peas': ['corn'],
#   'beans': ['eggplant', 'okra'],
#   'raddish': ['corn', 'cucumber'],
#   'okra': ['eggplant', 'cucumber', 'beans']
# }


plant_id_instance_map = {}
grid_pos_instance_map = {}


def CreatePlantsList(plant_dct, friends_map):
  plants_list = []
  for plant_id, num_selected in plant_dct.iteritems():
    p = datamodels.Plant(plant_id, num_selected)
    plant_id_instance_map[plant_id] = p
    p.friend_ids = friends_map[plant_id]
    plants_list.append(p)
  return plants_list


def CalculateNeighborCoordinates(x, y, length, breadth):
  neighbor_coordinates = []
  if y-1 in range(breadth):
    neighbor_coordinates.append((x, y-1))
  if x-1 in range(length):
    neighbor_coordinates.append((x-1, y))
  if x+1 in range(length):
    neighbor_coordinates.append((x+1, y))
  if y+1 in range(breadth):
    neighbor_coordinates.append((x, y+1))
  return neighbor_coordinates


def CreateGridList(length, breadth):
  grid_list = []
  for y in range(breadth):
    for x in range(length):
      g = datamodels.GridPlantLayout(x,y)
      g.neighbor_coordinates = CalculateNeighborCoordinates(
          x, y, length, breadth)
      g.available_neighbor_slots = len(g.neighbor_coordinates)
      grid_pos_instance_map[(x,y)] = g
      grid_list.append(g)
  return grid_list


def UpdateCurrentWeightage(plant_objs):
  for plant_obj in plant_objs:
    friend_ids = plant_obj.friend_ids
    num = 0
    for friend_id in friend_ids:
      obj = plant_id_instance_map.get(friend_id)
      if not obj:
        continue
      num += obj.GetAvailability()

    self_num = plant_obj.GetAvailability()
    if not self_num:
      plant_obj.current_weightage = 0
    else:
      if num == 0:
        plant_obj.current_weightage = 1
      else:
        plant_obj.current_weightage = math.ceil(1.0 * num / self_num)


def GetPlantWithHeighestWeightage(plant_objs):
  heighest_weighage = 0
  obj = None
  for plant_obj in plant_objs:
    if plant_obj.current_weightage > heighest_weighage:
      heighest_weighage = plant_obj.current_weightage
      obj = plant_obj
  return obj or plant_objs[0]


def GetFirstAvailableCell(plant_obj, grid_objs):
  if plant_obj.current_weightage > 4:
    max_weightage = 4
  else:
    max_weightage = plant_obj.current_weightage
  for grid_obj in grid_objs:
    if grid_obj.available_neighbor_slots == max_weightage and grid_obj.associated_plant is None:
      return grid_obj
  return None


def GetFirstGoodCell(plant_obj, grid_objs):
  good_cell = None
  available_boundaries = 0
  nearby_friends = 0
  for grid_obj in grid_objs:
    # print 'new grid cell'
    # print grid_obj.posx
    # print grid_obj.posy
    if grid_obj.associated_plant is not None:
      continue

    if good_cell is None:
      good_cell = grid_obj
      available_boundaries = grid_obj.available_neighbor_slots

      neighbor_coordinates = grid_obj.neighbor_coordinates
      neighbor_objs = [grid_pos_instance_map[n] for n in neighbor_coordinates]
      for o in neighbor_objs:
        if o.associated_plant and o.associated_plant.plant_id in plant_obj.friend_ids:
          nearby_friends += 1
      # print 'CASE 1'
      # print available_boundaries
      # print nearby_friends
      # print good_cell.posx
      # print good_cell.posy

    else:
      a = grid_obj.available_neighbor_slots
      if a > available_boundaries:
        available_boundaries = a
        good_cell = grid_obj
        neighbor_coordinates = grid_obj.neighbor_coordinates
        neighbor_objs = [grid_pos_instance_map[n] for n in neighbor_coordinates]
        nearby_friends = 0
        for o in neighbor_objs:
          if o.associated_plant and o.associated_plant.plant_id in plant_obj.friend_ids:
            nearby_friends += 1

        # print 'CASE 2'
        # print available_boundaries
        # print nearby_friends
        # print good_cell.posx
        # print good_cell.posy
      elif a == available_boundaries:
        neighbor_coordinates = grid_obj.neighbor_coordinates
        neighbor_objs = [grid_pos_instance_map[n] for n in neighbor_coordinates]
        nby_friends = 0
        for o in neighbor_objs:
          if o.associated_plant and o.associated_plant.plant_id in plant_obj.friend_ids:
            nby_friends += 1
        if nby_friends > nearby_friends:
          nearby_friends = nby_friends
          good_cell = grid_obj

          # print 'CASE 3'
          # print available_boundaries
          # print nearby_friends
          # print good_cell.posx
          # print good_cell.posy

  return good_cell


def UpdateAvailableNeighborSlots(grid_obj):
  for neighbor_coordinate in grid_obj.neighbor_coordinates:
    grid_pos_instance_map[neighbor_coordinate].available_neighbor_slots -= 1


def PlantATree(grid_obj, plant_obj):
  grid_obj.associated_plant = plant_obj
  UpdateAvailableNeighborSlots(grid_obj)
  plant_obj.planted_grid_pos.append(grid_obj)
  datamodels.selection_done += 1
  # print 'planted'
  # print plant_obj.plant_id
  # print grid_obj.posx
  # print grid_obj.posy


def PlantNeighbors(grid_obj, plant_obj):
  friend_ids = plant_obj.friend_ids
  friend_id_weightage_map = {}
  for friend_id in friend_ids:
    p1 = plant_id_instance_map.get(friend_id)
    if not p1:
      continue

    if p1.GetAvailability() > 0:
      friend_id_weightage_map[friend_id] = p1.current_weightage

  friend_id_weightage_map = sorted(
      friend_id_weightage_map.items(), key=operator.itemgetter(1), reverse=True)
  friends_in_order = [k for k,v in friend_id_weightage_map]
  friends_to_consider = []
  for f in friends_in_order:
    d = plant_id_instance_map.get(f)
    if d is None:
      continue
    a = plant_id_instance_map[f].GetAvailability()
    for i in range(a):
      friends_to_consider.append(f)
  idx = 0
  for neighbor_coordinate in grid_obj.neighbor_coordinates:
    if len(friends_to_consider) <= idx:
      return
    g = grid_pos_instance_map[neighbor_coordinate]
    if g.associated_plant is None:
      p1 = plant_id_instance_map.get(friends_to_consider[idx])
      if not p1:
        continue
      p = plant_id_instance_map[friends_to_consider[idx]]
      PlantATree(g, p)
      idx += 1


def RunOptimizer(length, breadth, selected_plants, friends_map):
  datamodels.total_selection = sum(selected_plants.values())
  datamodels.selection_done = 0
  plant_objs = CreatePlantsList(selected_plants, friends_map)
  grid_objs = CreateGridList(length, breadth)
  while datamodels.selection_done < datamodels.total_selection:
    UpdateCurrentWeightage(plant_objs)
    plant_obj = GetPlantWithHeighestWeightage(plant_objs)
    # print 'plant with heighest_weighage', plant_obj.plant_id

    availability = plant_obj.GetAvailability()
    # import pdb; pdb.set_trace()
    for m in range(availability):
      grid_obj = GetFirstAvailableCell(plant_obj, grid_objs)
      if grid_obj is None:
        grid_obj = GetFirstGoodCell(plant_obj, grid_objs)
      PlantATree(grid_obj, plant_obj)
      PlantNeighbors(grid_obj, plant_obj)

  results = []
  for g in grid_objs:
    results.append(g.associated_plant.plant_id)
  return results

if __name__ == '__main__':
  RunOptimizer()